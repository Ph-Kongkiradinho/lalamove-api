const express = require("express");
const { stringify } = require("querystring");
const app = express();

const port = process.env.PORT || 3000;

app.route("").get((req, res) => {
  const crypto = require("crypto");
  const axios = require("axios");
  const KEY = "pk_test_ec64b2b2ffa9a7b831f614fb44e1aded";
  const SECRET =
    "sk_test_SCHjBY9GjqjxsE6Tf6P+9uqOZqiIkY0fd3slfJQaX2+0cQoC5L24/g3KQd0xfuTS";
  const time = new Date().getTime().toString();
  const method = "POST";
  const path = "/v2/quotations";
  const body = {
    scheduleAt: "2022-01-15T18:30:00.00Z",
    serviceType: "MOTORCYCLE",
    stops: [
      {
        location: {
          lat: "13.740167",
          lng: "100.535237",
        },
        addresses: {
          th_TH: {
            displayString:
              "444 ถนน พญาไท แขวง วังใหม่ เขต ปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย",
            market: "TH_BKK",
          },
        },
      },
      {
        location: {
          lat: "14.740167",
          lng: "101.535237",
        },
        addresses: {
          th_TH: {
            displayString:
              "555 ถนน พญาไท แขวง วังใหม่ เขต ปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย",
            martket: "TH_BKK",
          },
        },
      },
    ],
    deliveries: [
      {
        toStop: 1,
        toContact: {
          name: "Customer",
          phone: "+6637013701",
        },
        remarks: "ORDER#94\r\n1. Tshirt จำนวน 1\r\n2. Hoodie จำนวน 1\r\n",
      },
    ],
    requesterContact: {
      name: "Owner",
      phone: "+6637013701",
    },
    specialRequests: ["COD", "HELP_BUY", "LALABAG"],
  };
  const rawSignature = `${time}\r\n${method}\r\n${path}\r\n\r\n${JSON.stringify(
    body
  )}`;
  const hash = crypto
    .createHmac("sha256", SECRET)
    .update(rawSignature)
    .digest("hex");
  const SIGNATURE = hash;
  const hmac = `hmac ${KEY}:${time}:${SIGNATURE}`;
  axios
    .post("https://rest.sandbox.lalamove.com/v2/quotations", body, {
      headers: {
        Authorization: hmac,
        "X-LLM-Market": `TH_BKK`,
        "X-Request-ID": crypto.randomBytes(16).toString("hex"),
        "Content-Type": "application/json",
      },
    })
    .then(function (response) {
      console.log("success");
      console.log(response);
    })
    .catch(function (error) {
      console.log("error");
      console.log("data: " + JSON.stringify(error.response.data));
      console.log("status: " + JSON.stringify(error.response.status));
    });
});

app.listen(port, () => {
  console.log("server is running on port: ", port);
});
